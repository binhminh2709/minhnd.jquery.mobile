<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<!-- Include meta tag to ensure proper rendering and touch zooming -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Include jQuery Mobile stylesheets -->
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
<!-- Include the jQuery library -->
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Include the jQuery Mobile library -->
<script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
</head>
<body>
  <div data-role="page" id="pageone">
    <div data-role="header">
      <h1>Welcome To My Homepage</h1>
    </div>
    
    <div data-role="main" class="ui-content">
      <p>Welcome! If you click on the link below, it will take you to Page Two.</p>
      <a href="#pagetwo">Go to Page Two "Mobile Button Icons"</a>
        
      <div data-role="controlgroup" data-type="horizontal">
        <p>Horizontal group:</p>
        <a href="#" class="ui-btn">Button 1</a> 
        <a href="#" class="ui-btn">Button 2</a>
        <a href="#" class="ui-btn">Button 3</a>
      </div>
      <br>
        
      <div data-role="controlgroup" data-type="vertical">
        <p>Vertical group (default):</p>
        <a href="#" class="ui-btn">Button 1</a>
        <a href="#" class="ui-btn">Button 2</a>
        <a href="#" class="ui-btn">Button 3</a>
      </div>
      
      <p>Normal Buttons:</p>
      <a href="#" class="ui-btn">Gray Button (default)</a>
      <a href="#" class="ui-btn ui-btn-b">Black Button</a>
      <br>
    
      <p>Inline Buttons:</p>
      <a href="#" class="ui-btn ui-btn-inline">Gray Button (default)</a>
      <a href="#" class="ui-btn ui-btn-inline ui-btn-b">Black Button</a>
      
      <p>Buttons with and without rounded corners:</p>
      <a href="#" class="ui-btn ui-corner-all">Button 1</a>
      <a href="#" class="ui-btn">Button 2</a>
      <br>
    
      <p>Inline Buttons with and without rounded corners:</p>
      <a href="#" class="ui-btn ui-btn-inline ui-corner-all">Button 2</a>
      <a href="#" class="ui-btn ui-btn-inline">Button 2</a>
    
    </div>
    
    <div data-role="footer">
      <h1>Footer Text</h1>
    </div>
  </div>
    
  <div data-role="page" id="pagetwo">
    <div data-role="header"><h1>Welcome To My Page: Mobile Button Icons</h1></div>
    
    <div data-role="main" class="ui-content">
      <p>This is Page Two. If you click on the link below, it will take you to Page One.</p>
      <a href="#pageone">Go to Page One</a><br>
      <a href="#pagethree">Go to Page Three</a><br>
      
      
      <a href="#" class="ui-btn ui-icon-arrow-l ui-btn-icon-left ui-btn-inline">Left Arrow</a>
      <a href="#" class="ui-btn ui-icon-arrow-r ui-btn-icon-left ui-btn-inline">Right Arrow</a>
      <a href="#" class="ui-btn ui-icon-info ui-btn-icon-left ui-btn-inline">Information</a>
      <a href="#" class="ui-btn ui-icon-delete ui-btn-icon-left ui-btn-inline">Delete</a>
      
      <br>
      <p>Positioning Icons:</p>
      <a href="#" class="ui-btn ui-icon-search ui-btn-icon-top ui-btn-inline">Top</a>
      <a href="#" class="ui-btn ui-icon-search ui-btn-icon-right ui-btn-inline">Right</a>
      <a href="#" class="ui-btn ui-icon-search ui-btn-icon-bottom ui-btn-inline">Bottom</a>
      <a href="#" class="ui-btn ui-icon-search ui-btn-icon-left ui-btn-inline">Left</a>
      
      <br>
      <p>A white "search" icon (default):</p>
      <a href="#" class="ui-btn ui-btn-inline ui-icon-search ui-btn-icon-left ui-corner-all ui-shadow">White icon (default)</a>  
      <a href="#" class="ui-btn ui-btn-inline ui-icon-search ui-btn-icon-notext ui-corner-all ui-shadow">White icon (default)</a>
  
      <p>A black "search" icon (class="ui-alt-icon"):</p>
      <a href="#" class="ui-btn ui-btn-inline ui-icon-search ui-btn-icon-left ui-corner-all ui-shadow ui-alt-icon">Black icon</a>   
      <a href="#" class="ui-btn ui-btn-inline ui-icon-search ui-btn-icon-notext ui-corner-all ui-shadow ui-alt-icon">Black icon</a>
      
      <p>A black "search" icon without the gray circle (combining "ui-nodisc-icon" and "ui-alt-icon"):</p>
      <a href="#" class="ui-btn ui-btn-inline ui-icon-search ui-btn-icon-left ui-corner-all ui-shadow ui-nodisc-icon ui-alt-icon">Black icon</a>   
      <a href="#" class="ui-btn ui-btn-inline ui-icon-search ui-btn-icon-notext ui-corner-all ui-shadow ui-nodisc-icon ui-alt-icon">Black icon</a>
      
      <br>
      <p>Click on the image to enlarge it.</p>
      <p>Notice the dark overlay.</p>
      <a href="#myPopup" data-rel="popup" data-position-to="window">
      <img src="http://gdurl.com/wSZb" alt="Skaret View" style="width:200px;"></a>
      
      <div data-role="popup" id="myPopup" data-overlay-theme="b">
        <p>This is my picture!</p> 
        <a href="#pageone" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
        <img src="http://gdurl.com/wSZb" style="width:1000px;" alt="Skaret View">
      </div>
      
      
    </div>
    
    <div data-role="footer"><h1>Footer Text</h1></div>
  </div>
  
  <div data-role="page" id="pagethree">
    <div data-role="header">
        <a href="#" class="ui-btn ui-icon-home ui-btn-icon-left">Home</a>
        <h1>Welcome To My Page: Mobile Toolbars</h1>
        <a href="#" class="ui-btn ui-icon-search ui-btn-icon-left">Search</a>
    </div>
    
    <div data-role="main" class="ui-content">
      <p>This is Page Two. If you click on the link below, it will take you to Page One.</p>
      <a href="#pageone">Go to Page One</a><br>
    </div>
  
    
    <div data-role="footer" style="text-align:center;">
        <a href="#" class="ui-btn ui-icon-plus ui-btn-icon-left">Add Me On Facebook</a>
        <a href="#" class="ui-btn ui-icon-plus ui-btn-icon-left">Add Me On Twitter</a>
        <a href="#" class="ui-btn ui-icon-plus ui-btn-icon-left">Add Me On Instagram</a>
    </div>
  </div>
  
  <div data-role="page" id="pagefor">
  </div>

</body>
</html>